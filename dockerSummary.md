#### Docker Summary

* Docker is a platform for developing,running applications etc.
* It gives a seprate infrastructure to our applicationsso as to deliver our project quickly
* Using docker our infrastructure can be managed in the same ways our applications are managed. 
* One major advantage is the reduction in delay between between prototyping and production.

#### Docker service providers
* There are many companies providing docker services few of the well known ones are
  * Amazon web service
  * Microsoft Azure
  * Digital ocean 

#### Docker platform
* Develop your application and its supporting components using containers.
* The container becomes the unit for distributing and testing your application.

### Docker Terminology

* Sandbox
 *The term ‘sandbox’ denotes the computing environment with which whatever happens inside sandbox remains in it. If you are going to perform ‘rm –rf’ inside sandbox, sandbox contents will be erased but the host machine that has sandbox undergoes zero damage. 

* Docker Container
 * The Docker container is dynamic implementation of Docker image. Multiple containers can easily be initiated from a single Docker image. Containers provide needed consistency for DevOps (development and operations). 

* Docker Images
 * Docker image can be compared to architectural sketch of house. Similarly, the container is house that is built with this drawing. The perfect analogy could be OOP (Object Oriented Programming), where the Docker image is compared to class and the 

* Docker Repository
 * A Docker repository is an internet based or network based service which holds Docker images. Docker images can either be pushed or pulled, from or to the Docker repository. You can either specify a Docker repository and can also create your own repository. There are two forms of Docker repository namely: private and public repository.

* Docker Hub
  * Docker Hub is a clustered resource mechanism while working with components of Docker technology. Docker Hub is a best example for public repository is Docker Hub and it helps you in collaborating with your friends to make most from Docker.

*Docker Daemon
  * Docker daemon runs on host system. The users cannot interact directly with Docker daemon but only through Docker clients.

* Docker Client
  * Docker Client is the chief user interfacing for Docker and it is in docker binary format. Docker daemon will accept the docker commands from users and establishes to and fro communication with Docker daemon.

*Docker Swarm
  *Docker Swarm is domestic cluster for Docker. This will allow creation and accessing to a collection of Docker hosts with the help of Docker tools.

#### Docker commands
  * docker run – Runs a command in a new container.
  * docker start – Starts one or more stopped containers
  * docker stop – Stops one or more running containers
  * docker build – Builds an image form a Docker file
  * docker pull – Pulls an image or a repository from a registry
  * docker push – Pushes an image or a repository to a registry
  * docker export – Exports a container’s filesystem as a tar archive
  * docker exec – Runs a command in a run-time container
  * docker search – Searches the Docker Hub for images
  * docker attach – Attaches to a running container
  * docker commit – Creates a new image from a container’s changes

#### Examples
1. Find the docker version.
```bash
docker –version
```
2. To pull images from the docker repository.
```bash
docker pull <image name>
```
3.This command is used to create a container from an image
```bash
Usage: docker run -it -d <image name>
```
4.This command is used to create a container from an image
```bash
Usage: docker run -it -d <image name>
```
5.This command is used to list the running containers
```bash
docker ps
```
6. This command is used to access the running container

```bash
docker exec -it <container id> bash
```
7.This command kills the container by stopping its execution immediately.
```bash
docker kill <container id>
```
8. This command creates a new image of an edited container on the local system
```bash
docker commit <conatainer id> <username/imagename>
```
#### Docker architecture 
*The docker architecture is illustarted in the figure below
![archi](https://i1.wp.com/www.docker.com/blog/wp-content/uploads/2019/06/948c0bc7-d107-486e-a2e6-59b131ee4e40-1.jpg?ssl=1)

#### Docker Workflow
* The Workflow adopted by comapnies while working with docker is illustarted below.
![workflow](https://collabnix.com/wp-content/uploads/2015/10/WithoutDocker_workflow.jpg)